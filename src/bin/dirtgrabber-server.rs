use chrono::NaiveDate;
use color_eyre::eyre::{eyre, Result, WrapErr};
use futures_util::{SinkExt, StreamExt};
use std::collections::HashMap;
use tokio::sync::broadcast;
use tokio_tungstenite::tungstenite::Message;

/// An external ID representing a campground
type CampgroundId = u64;

/// A map from day -> available count for the given campground
type CampgroundAvailabilities = (CampgroundId, HashMap<NaiveDate, u32>);

/// Broadcast notices of campsite availability to all connected Dirtgrabber clients, relying on
/// them to filter out non-applicable events.  This provides an easy way for clients to share
/// polling capacity, allowing multiple clients with the same queries to effectively reduce their
/// latency to notice new campsite availabilities.
///
/// Notably, we don't need to handle expiration or any form of persistent data, since all clients
/// are also polling the campgrounds at the same time.  Any stale or missed data on our part will
/// quickly be found by the clients, bringing everyone back to a accurate view of the world.
#[tokio::main]
async fn main() -> Result<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let server = tokio::net::TcpListener::bind("[::]:2991")
        .await
        .wrap_err("Could not bind to :2991")?;
    log::info!("Backend accepting connections on {}", server.local_addr()?);

    let (campground_broadcast, _) = tokio::sync::broadcast::channel::<CampgroundAvailabilities>(16);

    // Continuously handle client connections
    loop {
        if let Ok((connection, addr)) = server.accept().await {
            log::info!("Registered new connection from {}", addr);

            tokio::spawn(client_handler(connection, campground_broadcast.clone()));
        }
    }
}

/// Handle a single Dirtgrabber client.
/// Broadcast its messages when it finds new campground availabilities, and notify it when other
/// clients have found availabilities.
async fn client_handler(
    connection: tokio::net::TcpStream,
    campground_broadcast: broadcast::Sender<CampgroundAvailabilities>,
) -> Result<()> {
    let (mut client_write, mut client_read) =
        tokio_tungstenite::accept_async(connection).await?.split();

    let mut campground_notification = campground_broadcast.subscribe();

    // Track what the last message we received was.  This provides an imperfect way to avoid
    // notifying a client about something we were just notified about.
    let mut last_message = Default::default();

    loop {
        tokio::select! {
            client_data = client_read.next() => {
                // We got a message from the client.
                match client_data.ok_or_else(|| eyre!("Failed to read data from client"))?? {
                    Message::Close(_) => break,
                    Message::Ping(data) => {
                        client_write.send(Message::Pong(data)).await
                            .wrap_err("Failed to pong to client")?;
                    }
                    Message::Pong(_) => (),
                    Message::Text(msg) => {
                        let query: CampgroundAvailabilities = serde_json::from_str(&msg)
                            .wrap_err("Invalid client results")?;

                        log::info!("{{ campground: {}, availabilities: {:?} }}", query.0, query.1);
                        last_message = query.clone();

                        let _ = campground_broadcast.send(query)
                            .expect("Must be at least one active receiver (us)");
                    }
                    unknown => log::error!("Unexpected client data: {}", unknown),
                };
            },

            new_campgrounds = campground_notification.recv() => {
                let new_campgrounds = new_campgrounds.wrap_err("Lagged behind broadcaster")?;

                if new_campgrounds != last_message {
                    // We were notified about a new campground availability.  Send it back to the
                    // client.
                    client_write.send(Message::Text(
                            serde_json::to_string(&new_campgrounds)
                            .wrap_err("Failed to serialize campground info")?
                    )).await.wrap_err("Client failed to receive ping")?;
                }
            }
        }
    }

    Ok(())
}
