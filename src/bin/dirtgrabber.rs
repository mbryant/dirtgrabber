use chrono::{Datelike, NaiveDate};
use clap::Parser;
use color_eyre::eyre::{eyre, Result, WrapErr};
use futures::StreamExt;
use itertools::Itertools;
use owo_colors::OwoColorize;
use serde::Deserialize;
use std::collections::{HashMap, HashSet};
use std::time::Duration;
use tokio::sync::{mpsc, Mutex};

///////////////////////////////////////////////////////////////////////////////
// External API
///////////////////////////////////////////////////////////////////////////////
type ApiId = u64;
type ApiDate = chrono::DateTime<chrono::Utc>;

#[derive(Deserialize)]
struct CampgroundOverview {
    /// The recreation.gov id for the campground
    #[serde(
        rename = "entity_id",
        deserialize_with = "serde_aux::field_attributes::deserialize_number_from_string"
    )]
    id: ApiId,

    /// The campground's name
    name: String,

    /// We can conceptually use this as a proxy for how likely people are to have booked this
    /// campground (bigger numbers should correspond to larger and more popular campgrounds).  In
    /// turn, we can use this information to poll popular campgrounds more frequently, since there
    /// are higher odds of cancellations.
    number_of_ratings: Option<u32>,
}

#[derive(Deserialize)]
struct CampgroundSeason {
    season_start: ApiDate,
    season_end: ApiDate,
}

#[derive(Deserialize)]
struct CampgroundInfo {
    campsites: HashMap<ApiId, CampsiteInfo>,
}

#[derive(Deserialize)]
struct CampsiteInfo {
    availabilities: HashMap<ApiDate, String>,
    quantities: HashMap<ApiDate, i32>,
}

///////////////////////////////////////////////////////////////////////////////
// Args
///////////////////////////////////////////////////////////////////////////////
#[derive(Parser)]
#[command(author, version, about)]
#[command(next_line_help = false)]
struct Args {
    /// The first date (inclusive) to look for campsites, in yyyy-mm-dd format
    #[arg(long, value_parser = parse_date)]
    first_date: Option<NaiveDate>,

    /// The last date (inclusive) to look for campsites, in yyyy-mm-dd format
    #[arg(long, value_parser = parse_date)]
    last_date: Option<NaiveDate>,

    /// Polling frequency (total requests per minute)
    #[arg(short, long, default_value_t = 19)]
    frequency: u64,

    /// Only check for campsites once before exiting
    #[arg(long)]
    oneshot: bool,

    /// The park id to look for campsites in (defaults to Yosemite)
    #[arg(short, long, default_value = "2991")]
    park_id: ApiId,

    /// A list of campgrounds ids to ignore (defaults to Wawona Horse Campground)
    #[arg(short, long, default_values = ["10220609"])]
    ignored_campgrounds: Vec<ApiId>,

    /// In balanced query mode, campground lookups are evenly distributed across campgrounds.
    /// In unbalanced mode (default), they're sent unevenly in order to query the most popular
    /// campsites more frequently.
    #[arg(long)]
    balanced_queries: bool,

    /// A server to connect to in order share campground results.
    /// Setting this to an invalid domain will disable this functionality.
    #[arg(long, default_value = "dirtgrabber.programsareproofs.com/socket")]
    server: String,
}

fn parse_date(date: &str) -> Result<NaiveDate> {
    let date = NaiveDate::parse_from_str(date, "%Y-%m-%d")
        .wrap_err("must be a valid date in yyyy-mm-dd format")?;

    if date < chrono::offset::Local::now().date_naive() {
        Err(eyre!("must be in the future"))
    } else {
        Ok(date)
    }
}

///////////////////////////////////////////////////////////////////////////////
// Core logic
///////////////////////////////////////////////////////////////////////////////
/// Represents a query for available campsites
struct CampgroundQuery {
    /// The campground to query for
    id: ApiId,

    /// The campground's human name
    name: String,

    /// Each campground may be polled at a different interval, since we want to poll the most
    /// likely campgrounds more.
    interval: Duration,

    first_date: NaiveDate,
    last_date: NaiveDate,
}

/// A map from day -> available count for a given campground
type CampgroundAvailabilities = (ApiId, HashMap<NaiveDate, i32>);

/// A global client for talking to recreation.gov
static REQWEST_CLIENT: std::sync::LazyLock<reqwest::Client> = std::sync::LazyLock::new(||
    // Configure some HTTP settings that might speed our connections up a bit, as well as
    // setting a 3s timeout for all connections - everything on recreation.gov should
    // complete in <1s, so anything much longer is weird.
    reqwest::Client::builder()
        .https_only(true)
        .http2_prior_knowledge()
        .min_tls_version(reqwest::tls::Version::TLS_1_3)
        .timeout(Duration::from_secs(3))
        .build()
        .wrap_err("Invalid HTTP client configuration").unwrap());

#[tokio::main]
async fn main() -> Result<()> {
    color_eyre::install()?;
    let args = Args::parse();

    let first_date = args
        .first_date
        .unwrap_or_else(|| chrono::offset::Local::now().date_naive());
    let last_date = args
        .last_date
        .unwrap_or_else(|| first_date + chrono::Days::new(14));

    if first_date > last_date {
        return Err(eyre!(
            "First date ({}) can't be later than last date ({})",
            first_date,
            last_date
        ));
    }

    // First date to last date can only cross one month boundary, to ensure we only need two
    // queries to the recreation.gov api to find available campsites.  Checking for this is
    // slightly annoying.
    let years = last_date.year() - first_date.year();
    if years > 1
        || (years == 1 && (first_date.month0() + 1) % 12 != last_date.month0())
        || (years == 0 && last_date.month() - first_date.month() > 1)
    {
        return Err(eyre!(
            "First date ({}) and last date ({}) can't span more than one month.",
            first_date,
            last_date
        ));
    }

    // Load necessary startup information in parallel.
    let (campgrounds, park_name) = {
        #[derive(Deserialize)]
        struct CampgroundList {
            #[serde(rename = "results")]
            campgrounds: Vec<CampgroundOverview>,
        }

        #[derive(Deserialize)]
        struct ParkInfo {
            name: String,
        }

        let (campground_info, park_info, version_info) = tokio::join!(
            REQWEST_CLIENT
                .get(format!(
                    "https://www.recreation.gov/api/search?fq=parent_asset_id%3A{}&fq=entity_type%3Acampground",
                    args.park_id
                ))
                .send()
                .await
                .wrap_err("Failed to load campground list")?
                .json::<CampgroundList>(),
            REQWEST_CLIENT
                .get(format!(
                    "https://www.recreation.gov/api/external/inventory/gateway/{}",
                    args.park_id
                ))
                .send()
                .await
                .wrap_err("Failed to load park information")?
                .json::<ParkInfo>(),
            tokio::time::timeout(Duration::from_millis(500), check_latest::check_max_async!())
        );

        if let Ok(Ok(Some(version))) = version_info {
            println!("{}", format!("Dirtgrabber v{} has been released!  Consider updating to the newest release: {}", version, "cargo install dirtgrabber".italic()).bright_purple());
        }

        let park_name = park_info
            .wrap_err("Failed to decode park information")?
            .name;
        let all_campgrounds = campground_info
            .wrap_err("Failed to decode campground list")?
            .campgrounds;

        /// Load more information about the campground.  In particular, we want to know information
        /// about when the campground is open, so we can determine whether to query it or not.
        async fn campground_lookup(
            campground: CampgroundOverview,
        ) -> Result<(CampgroundOverview, Vec<CampgroundSeason>)> {
            #[derive(Deserialize)]
            struct CampgroundSeasons {
                rates_list: Vec<CampgroundSeason>,
            }

            let rates = REQWEST_CLIENT
                .get(format!(
                    "https://www.recreation.gov/api/camps/campgrounds/{}/rates",
                    campground.id
                ))
                .send()
                .await
                .wrap_err_with(|| {
                    format!(
                        "Failed to load campground rates for {} ({})",
                        campground.id, campground.name
                    )
                })?
                .json::<CampgroundSeasons>()
                .await
                .wrap_err_with(|| {
                    format!(
                        "Failed to parse campground rates for {} ({})",
                        campground.id, campground.name
                    )
                })?
                .rates_list;

            Ok((campground, rates))
        }

        // Determine which of the campgrounds are actually open during the dates we're looking for.
        let open_campgrounds = {
            let mut campground_loader = tokio::task::JoinSet::new();

            for lookup in all_campgrounds
                .into_iter()
                .filter(|campground| !args.ignored_campgrounds.contains(&campground.id))
                .map(campground_lookup)
            {
                campground_loader.spawn(lookup);
            }

            let mut open_campgrounds: HashMap<ApiId, CampgroundOverview> = HashMap::new();

            while let Some(loaded) = campground_loader.join_next().await {
                let (campground, rates) = loaded
                    .wrap_err("Campground loading crashed")?
                    .wrap_err("Failed to successfully load campground info")?;

                // Now we have campground information and rates.  Check if any of the rates cover
                // the time period we're looking for (meaning the campground is open).
                if rates.into_iter().any(|rate| {
                    rate.season_end
                        > first_date
                            .and_hms_opt(0, 0, 0)
                            .expect("00:00:00 should always be valid")
                            .and_utc()
                        && rate.season_start
                            < last_date
                                .and_hms_opt(0, 0, 0)
                                .expect("00:00:00 should always be valid")
                                .and_utc()
                }) {
                    open_campgrounds.insert(campground.id, campground);
                }
            }

            open_campgrounds
        };

        (open_campgrounds, park_name)
    };

    // Determine how frequently to poll each of the campgrounds while maintaining our target of
    // `args.frequency` queries per minute.  In balanced query mode, we'll poll each of the
    // campgrounds with the same frequency, but if we're not in balanced mode, we want to spend
    // more time polling the campsites with more opportunities.
    let update_intervals: HashMap<ApiId, f32> = {
        let relative_frequencies = campgrounds
            .iter()
            .map(|(&id, campground)| {
                (
                    id,
                    if args.balanced_queries {
                        1.0
                    } else {
                        // Lightly bias towards campsites with more ratings, using `log_2(x + 32)`
                        // to ensure campsites with few ratings aren't punished too much.
                        ((campground.number_of_ratings.unwrap_or_default() as f32) + 64.0).log2()
                    },
                )
            })
            .collect::<HashMap<ApiId, f32>>();
        let total_frequencies = relative_frequencies.values().sum::<f32>();

        let month_modifier = if first_date.month() != last_date.month() {
            // If we need to poll multiple months, we'll have to make twice the queries.  This will
            // correspondingly half our scanning rate in order to avoid being rate-limited.
            println!(
                "{} {}",
                "WARNING:".bold().red(),
                "Searching across a month boundary - this will half the scanning rate compared to searching within a single month.".bright_red()
            );
            2.0
        } else {
            1.0
        };

        relative_frequencies
            .into_iter()
            .map(|(id, frequency)| {
                (
                    id,
                    (60_000.0 / (args.frequency as f32) * (total_frequencies / frequency))
                        * month_modifier,
                )
            })
            .collect()
    };

    // Print a table of information about the campsites we'll be looking at.
    {
        let mut builder = tabled::builder::Builder::default();
        builder.push_record([
            "Campground".bold().underline().to_string(),
            "URL".bold().underline().to_string(),
            "Search frequency".bold().underline().to_string(),
        ]);

        for (name, id) in campgrounds.values().map(|c| (&c.name, &c.id)).sorted() {
            builder.push_record([
                name,
                &campground_url(id).blue().dimmed().to_string(),
                &format!("~{:<.1}s", update_intervals[id] / 1000.0),
            ]);
        }
        if campgrounds.is_empty() {
            builder.push_record(["No open campgrounds".red().to_string()]);
        }

        let mut table = builder.build();
        table
            .with(tabled::settings::Style::sharp())
            .with(tabled::settings::Panel::header(format!(
                "Searching reservable campgrounds in {} between {} and {}",
                park_name.green(),
                first_date.format("%B %d").green(),
                last_date.format("%B %d").green()
            )))
            .with(tabled::settings::Modify::new((0, 0)).with(tabled::settings::Alignment::center()))
            .with(tabled::settings::style::BorderSpanCorrection);
        println!("{}", table);
    }

    if campgrounds.is_empty() {
        return Ok(());
    }

    // We delegate campground polling to individual tasks, allowing each to have separate polling
    // criteria as well as determining when results have changed.
    // All campground bollers will send their results over a single MPSC to us.
    let (poller_notify, mut poller_receive) = mpsc::unbounded_channel();

    for campground in campgrounds.values() {
        tokio::spawn(campground_poller(
            CampgroundQuery {
                id: campground.id,
                name: campground.name.clone(),
                first_date,
                last_date,
                interval: Duration::from_millis(update_intervals[&campground.id] as u64),
            },
            poller_notify.clone(),
        ));
    }

    // Spawn a thread to listen to a centralized Dirtgrabber server and receive notifications from
    // other Dirtgrabbers about availabilities.
    let (readserver_notify, mut readserver_receive) = mpsc::unbounded_channel();
    let (writeserver_update, writeserver_receive) = mpsc::unbounded_channel();

    let mut server_available = if let Ok((socket, _)) =
        tokio_tungstenite::connect_async(&format!("wss://{}", args.server)).await
    {
        let (server_broadcast, server_read) = socket.split();

        // Spawn a poller that receives results from the Dirtgrabber server.
        tokio::spawn(server_poller(
            server_read,
            first_date,
            last_date,
            readserver_notify,
        ));

        // Spawn a task that updates the server with new data from us.
        tokio::spawn(server_notifier(server_broadcast, writeserver_receive));

        true
    } else {
        // We can't connect to the server.  We'll still return a channel to listen for server
        // results, but we'll never receive anything on it.
        timed_log!(
            "{}",
            format!(
                "Failed to connect to server wss://{} - sharing of results disabled",
                args.server
            )
            .bright_red()
        );

        false
    };

    // Track the set of previous availabilities for each campsite so we can tell when the data has
    // changed and we should re-render.
    let mut campground_info = campgrounds
        .keys()
        .map(|&id| (id, HashMap::new()))
        .collect::<HashMap<ApiId, _>>();
    let mut first = true;

    // Track the previous availabilities for each day so we can highlight any new rows.
    let mut prev_availabilities = HashMap::<NaiveDate, HashMap<ApiId, i32>>::new();

    // Periodically check all campgrounds for available sites and print them if the set of
    // available sites has changed.
    loop {
        // Whether the set of available sites changed during this iteration.
        // We'll mark this as true for the first iteration, since we always want to draw results
        // the first time.
        let mut campground_changed = first;
        first = false;

        // Wait ~indefinitely for some campground to return information, then batch together
        // responses within a window after that before updating the user.
        let mut window = tokio::time::Instant::now() + Duration::from_secs(3600);
        let mut woken = false;

        loop {
            tokio::select! {
                local = poller_receive.recv() => {
                    match local.ok_or(eyre!("All campground pollers exited"))? {
                        Ok((campground_id, available_spaces)) => {
                            if campground_info[&campground_id] != available_spaces {
                                // We got a different set of information about a campground.
                                campground_changed = true;

                                // Notify the central Dirtgrabber server about this new
                                // availability.
                                if server_available {
                                    // We don't care about failures here, since we'll detect issues
                                    // talking to the server on the read pathway.
                                    let _ = writeserver_update.send((campground_id, available_spaces.clone()));
                                }

                                campground_info.insert(campground_id, available_spaces);
                            }
                        }
                        Err(e) => {
                            timed_log!("{}: {:#}", "Error loading data".red(), e.red().dimmed());
                        }
                    }
                }

                server = readserver_receive.recv(), if server_available => {
                    if let Some((campground_id, availabilities)) = server {
                        assert!(!availabilities.is_empty(), "Server won't notify us about empty campgrounds");

                        // The server may send us information about campgrounds we aren't looking
                        // at, so we'll ignore that.
                        if let Some(existing) =  campground_info.get_mut(&campground_id) {
                            // Record that we have new availabilities in the campground, but don't
                            // overwrite existing availabilities since the server doesn't fully
                            // represent the truth.
                            if existing != &availabilities {
                                existing.extend(availabilities);
                                campground_changed = true;
                            }
                        }
                    } else {
                        // The server terminated in some fashion.  No use in listening to it anymore.
                        server_available = false;
                    }
                }

                _ = tokio::time::sleep_until(window) => {
                    // The window is over - time to render the changes!
                    break;
                }
            }

            if !woken {
                // This is our first time waking up, so now we're trying to aggregate responses
                // within a short window.
                window = tokio::time::Instant::now() + Duration::from_millis(800);
                woken = true;
            }
        }

        if campground_changed {
            timed_log!(
                "{}",
                "Campsite status changed! New entries are bolded.".bright_cyan()
            );

            // Rebuild the full table of availabilities
            let availabilities = {
                let mut availabilities = HashMap::new();

                for (&campground_id, available_spaces) in campground_info.iter() {
                    for (&date, &count) in available_spaces.iter() {
                        availabilities
                            .entry(date)
                            .or_insert_with(HashMap::new)
                            .insert(campground_id, count);
                    }
                }

                availabilities
            };

            let mut builder = tabled::builder::Builder::default();
            builder.push_record([
                "Date".bold().underline().to_string(),
                "Available spaces".bold().underline().to_string(),
                "Campground".bold().underline().to_string(),
                "URL".bold().underline().to_string(),
            ]);

            for date in availabilities.keys().sorted() {
                let existing_date = prev_availabilities.contains_key(date);

                for (campground_id, &count) in availabilities[date].iter().sorted() {
                    // This is a new campsite if we didn't have any campsites available on this
                    // date or this campsite wasn't previously available on it.
                    let new_campsite =
                        !existing_date || !prev_availabilities[date].contains_key(campground_id);
                    let url = campground_url(campground_id);

                    let count = if count == 0 {
                        // Sometimes the API gives us back garbage.  Roll with it.
                        "unknown".to_string()
                    } else {
                        format!("{}", count)
                    };

                    if new_campsite {
                        // Print new campsites in bold.
                        builder.push_record([
                            format!(
                                "{} {}",
                                date.format("%b %e").bold(),
                                date.format("(%a)").dimmed()
                            ),
                            count.bold().to_string(),
                            campgrounds[campground_id].name.bold().to_string(),
                            url.bright_blue().bold().to_string(),
                        ]);
                    } else {
                        builder.push_record([
                            format!(
                                "{} {}",
                                date.format("%b %e").bright_black(),
                                date.format("(%a)").bright_black().dimmed()
                            ),
                            count.bright_black().to_string(),
                            campgrounds[campground_id].name.bright_black().to_string(),
                            url.blue().dimmed().to_string(),
                        ]);
                    }
                }
            }

            if availabilities.is_empty() {
                builder.push_record([format!("{}", "No availabilities".red())]);
            }

            let mut table = builder.build();
            table
                .with(tabled::settings::Panel::header(format!(
                    "Available {} campsites",
                    park_name.green(),
                )))
                .with(tabled::settings::Style::sharp())
                .with(
                    tabled::settings::Modify::new((0, 0))
                        .with(tabled::settings::Alignment::center()),
                )
                .with(tabled::settings::style::BorderSpanCorrection);
            println!("{}", table);

            prev_availabilities = availabilities;
        }

        if args.oneshot {
            break;
        }
    }

    Ok(())
}

/// Repeatedly poll a campground according to the given query instructions, returning data back to
/// the caller over a channel.
async fn campground_poller(
    query: CampgroundQuery,
    channel: mpsc::UnboundedSender<Result<CampgroundAvailabilities>>,
) -> Result<()> {
    async fn load_campground(
        campground_id: ApiId,
        urls: &'static [String],
        first_date: NaiveDate,
        last_date: NaiveDate,
    ) -> Result<CampgroundAvailabilities> {
        static RATE_LIMITED: Mutex<()> = Mutex::const_new(());

        assert!(
            urls.len() <= 2,
            "More than two months at a time isn't supported"
        );

        async fn load_month(url: &String) -> Result<Option<CampgroundInfo>> {
            let response =
                REQWEST_CLIENT.get(url).send().await.wrap_err_with(|| {
                    format!("Error loading campground information from {}", url)
                })?;

            let response = match response.status() {
                reqwest::StatusCode::TOO_MANY_REQUESTS | reqwest::StatusCode::FORBIDDEN => {
                    // We were rate-limited.  Return that we were successful and don't need to
                    // retry, but that we didn't find any results.
                    //
                    // We also halt all other queries to allow the rate-limiter to calm down and
                    // let us through.

                    // If we don't get the lock immediately, it's because someone else is holding
                    // the rate-limter already.
                    if let Ok(_) = RATE_LIMITED.try_lock() {
                        timed_log!("{}", "Rate-limiting detected.  Backing off...".red());
                        tokio::time::sleep(
                            // Experimentally it can take a while for the rate-limiter to reset.
                            Duration::from_secs(120),
                        )
                        .await;
                        timed_log!(
                            "{}",
                            "Waited out the rate limiting, resuming queries...".green()
                        );
                    }

                    return Ok(None);
                }
                _ => response.error_for_status()?,
            };

            response
                .json::<CampgroundInfo>()
                .await
                .wrap_err("Invalid campground information from api")
                .map(Some)
        }

        let mut campground_info = {
            {
                // Try to acquire the rate-limiting lock, which will block if we're currently being
                // rate-limited.
                let _ = RATE_LIMITED.lock().await;
            }

            // Load information for each of the campgrounds in parallel
            let lookup_handles: Vec<_> = futures::stream::iter(urls)
                .map(|url| {
                    tokio::spawn(async {
                        // Retry the entire load path on failures.  It's important to do this here
                        // rather than just around the request, since we very frequently receive
                        // invalid json from the API which is fixed on retry.
                        // We avoid retrying much, as beyond the spurious failures the most likely
                        // reason to fail is because we've been rate limited.
                        tokio_retry::Retry::spawn(
                            tokio_retry::strategy::ExponentialBackoff::from_millis(50).take(2),
                            || load_month(url),
                        )
                        .await
                        .and_then(|x| {
                            // If we find an entry of the form Ok(None), it means we were
                            // rate-limited.
                            x.ok_or(eyre!("Ratelimiting detected".bright_red()))
                        })
                    })
                })
                .collect()
                .await;

            let mut campground_info = Vec::with_capacity(lookup_handles.len());

            for handle in lookup_handles {
                campground_info.push(handle.await??);
            }

            campground_info
        };

        let mut available_dates = HashSet::<NaiveDate>::new();
        let mut capacities = HashMap::new();

        for info in campground_info.iter_mut() {
            for campsite in info.campsites.values_mut() {
                // We're only interested in dates that have availabilities.
                // TODO: It would be interesting to see what other statuses exist - maybe we're
                // interested in some?
                campsite
                    .availabilities
                    .retain(|_, status| status == "Available");

                available_dates.extend(
                    campsite
                        .availabilities
                        .iter()
                        .map(|(&date, _)| date.date_naive())
                        .filter(|&date| date >= first_date && date <= last_date),
                );
                for (date, capacity) in campsite
                    .quantities
                    .iter()
                    .filter(|(date, _)| campsite.availabilities.contains_key(date))
                {
                    *capacities.entry(date.date_naive()).or_insert(0) += capacity;
                }
            }
        }

        Ok((
            campground_id,
            available_dates
                .into_iter()
                .map(|date| {
                    // The API sometimes gives us back empty `capacities`.  We fill with a default
                    // value when this happens, since `availabilities` appears to be the real source of
                    // truth.
                    (date, *capacities.get(&date).unwrap_or(&0))
                })
                .collect(),
        ))
    }

    let urls: &'static [String] = if query.first_date.month() != query.last_date.month() {
        // There are multiple months we need to query
        vec![query.first_date, query.last_date]
    } else {
        vec![query.last_date]
    }
    .into_iter()
    .map(|date| {
        format!(
            "https://www.recreation.gov/api/camps/availability/campground/{}/month?start_date={}",
            query.id,
            // The API requires this parameter to be the first day of the month we're looking for
            urlencoding::encode(&format!("{}-01T00:00:00.000Z", date.format("%Y-%m")))
        )
    })
    .collect::<Vec<_>>()
    // These last forever, so leak them to simplify lifetimes.
    .leak();

    // Ensure we do our lookups at a roughly constant frequency, regardless of how long the
    // previous lookup might've taken.
    let mut interval = tokio::time::interval(query.interval);

    // If we miss a tick, it's better to skip than to try to load campground info multiple times to
    // "catch up".
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    loop {
        // First tick is immediate
        interval.tick().await;

        let available_space = load_campground(query.id, urls, query.first_date, query.last_date)
            .await
            .wrap_err_with(|| format!("Bad information returned from {}", query.name));

        channel
            .send(available_space)
            .wrap_err("Failed to send to coordinator")?;
    }
}

///////////////////////////////////////////////////////////////////////////////
// Dirtgrabber Server Coordination
///////////////////////////////////////////////////////////////////////////////

/// Listen to a Dirtgrabber server for availabilities within our given date range.
async fn server_poller(
    mut server_read: futures::stream::SplitStream<
        tokio_tungstenite::WebSocketStream<
            tokio_tungstenite::MaybeTlsStream<tokio::net::TcpStream>,
        >,
    >,
    first_date: NaiveDate,
    last_date: NaiveDate,
    channel: mpsc::UnboundedSender<CampgroundAvailabilities>,
) -> Result<()> {
    /// Tiny helper function to make it easier to display errors from the server.
    async fn reader(
        server_read: &mut futures::stream::SplitStream<
            tokio_tungstenite::WebSocketStream<
                tokio_tungstenite::MaybeTlsStream<tokio::net::TcpStream>,
            >,
        >,
    ) -> Result<CampgroundAvailabilities> {
        let msg = server_read
            .next()
            .await
            .ok_or(eyre!("Server disconnected"))?;

        serde_json::from_slice::<CampgroundAvailabilities>(
            &msg.wrap_err("Failed to read message from server")?
                .into_data(),
        )
        .wrap_err("Failed to parse message from server")
    }

    loop {
        match reader(&mut server_read).await {
            Ok((campground, availabilities)) => {
                // Filter the availabilities to only the dates we care about.
                let inrange = availabilities
                    .into_iter()
                    .filter(|&(date, _)| date >= first_date && date <= last_date)
                    .collect::<HashMap<NaiveDate, i32>>();

                if !inrange.is_empty() {
                    channel
                        .send((campground, inrange))
                        .expect("Coordinator must be running");
                }
            }
            Err(e) => {
                timed_log!(
                    "{}: {}",
                    "Error: lost connection to server".red(),
                    e.dimmed()
                );

                return Err(e);
            }
        }
    }
}

/// Pass updates from clients to the centralized Dirtgrabber server.
/// This is basically just passing through from a channel, but makes it simpler for us to handle
/// cases where the server crashes/etc since we only need to work with the channel api.
async fn server_notifier(
    mut server_notify: futures::stream::SplitSink<
        tokio_tungstenite::WebSocketStream<
            tokio_tungstenite::MaybeTlsStream<tokio::net::TcpStream>,
        >,
        tokio_tungstenite::tungstenite::Message,
    >,
    mut channel: mpsc::UnboundedReceiver<CampgroundAvailabilities>,
) -> Result<()> {
    use futures_util::SinkExt;

    loop {
        let update = channel.recv().await;

        server_notify
            .send(tokio_tungstenite::tungstenite::Message::Text(
                serde_json::to_string(&update).expect("Availabilities must be serializable"),
            ))
            .await
            .wrap_err("Failed to send update to server")?;
    }
}

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
/// Returns the recreation.gov URL for the given campground
fn campground_url(id: &ApiId) -> String {
    format!("https://www.recreation.gov/camping/campgrounds/{}", id)
}

/// Print the current time, then the message we're trying to log.
#[macro_export]
macro_rules! timed_log {
    ( $format:expr, $($args: expr),* ) => {
        let now = chrono::offset::Local::now();
        println!(
            concat!("[{}] ", $format),
            now.format("%m-%d %H:%M:%S").yellow(),
            $(
                $args
            ),*
        );
    }
}
