![Dirtgrabber logo](dirtgrabber.png "Dirtgrabber")

# Dirtgrabber

A tool for figuring out which campsites are open in a National Park.

Made to improve the experience of using
[recreation.gov](https://recreation.gov) for people who care more about getting
a campsite to sleep in than any of its other aspects.

## Instructions

Install via `cargo install dirtgrabber`, then run as `dirtgrabber`.

Read the directions via `dirtgrabber --help` to find out about the more
interesting features.
